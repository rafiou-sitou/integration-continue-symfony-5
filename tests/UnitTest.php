<?php

namespace App\Tests;

use App\Entity\Blog;
use PHPUnit\Framework\TestCase;

class UnitTest extends TestCase
{
    public function testBlog(): void
    {
        $blog = (new Blog())->setTitle('Blog title');

        self::assertSame('Blog title', $blog->getTitle());
    }
}
