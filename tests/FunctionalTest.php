<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class FunctionalTest extends WebTestCase
{
    public function testShouldDisplayBlogIndex(): void
    {
        $client = static::createClient();
        $client->followRedirects();
        $client->request('GET', '/blog');

        self::assertResponseIsSuccessful();
        self::assertSelectorTextContains('h1', 'Blog index');
    }

    public function testShouldDisplayCreateNewDemo(): void
    {
        $client = static::createClient();
        $client->followRedirects();
        $client->request('GET', '/blog/new');

        self::assertResponseIsSuccessful();
        self::assertSelectorTextContains('h1', 'Create new Blog');
    }

    public function testShouldAddNewBlog(): void
    {
        $client = static::createClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/blog/new');

        $buttonCrawlerNode = $crawler->selectButton('Save');

//        $form = $buttonCrawlerNode->form();

        $uuid = uniqid('', true);

        $form = $buttonCrawlerNode->form([
            'blog[title]'    => 'Add Blog For Test ' . $uuid,
        ]);

        $client->submit($form);

        self::assertResponseIsSuccessful();
        self::assertSelectorTextContains('body', 'Add Blog For Test ' . $uuid);
    }
}
